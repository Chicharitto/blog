Rails.application.routes.draw do
  resources :subscriptions
  devise_for :users, controllers: { registrations: "my_devise/registrations" }
  resources :news, only: [:index, :show]
  namespace :mails do
    get :unsubscription
  end



  namespace :admin do
    get :dashboard, controller: 'dashboard'
    get :news_list, controller: 'dashboard'
    resources :categories
    resources :news
  end

  root 'news#index'
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
