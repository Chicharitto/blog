# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create(email: "admin@admin.ru", password: "qweqwe", is_admin: true, subscription: "not")

2.times do |i|
    User.create(email: "user#{i}@ya.ru", password: "qweqwe", subscription: "not")
end

3..4.times do |i|
    User.create(email: "user#{i}@ya.ru", password: "qweqwe", subscription: "every_day")
end

3..4.times do |i|
    Subscription.create(user_id: 3, category_id: i)
    Subscription.create(user_id: 4, category_id: i+1)
end

5..6.times do |i|
    User.create(email: "user#{i}@ya.ru", password: "qweqwe", subscription: "every_week")
end

5..6.times do |i|
    Subscription.create(user_id: 5, category_id: i)
    Subscription.create(user_id: 6, category_id: i+1)
end


5.times do |i|
    Category.create(title: "Категория #{i}")
end

5.times do |i|
    News.create(category_id: i, title: "Новость#{i}", text: "Очень интересный материал")
end