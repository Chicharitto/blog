class RenameColumnInUserTable < ActiveRecord::Migration[5.2]
  def change
    change_column :users, :is_admin, :boolean, default: false
    rename_column :users, :select_category, :unsubscription_token
  end
end
