class ChangeUsersTable < ActiveRecord::Migration[5.2]
  def change
    change_table :users do |t|
      t.string :subscription, default: "every_day"
      t.string :select_category
    end

    change_table :news do |t|
      t.belongs_to :category, index: true
    end
  end
end
