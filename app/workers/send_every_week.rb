class SendEveryWeek < ApplicationWorker
  sidekiq_options queue: "mailer"
  
  def perform
    User.where(subscription: "every_week").find_each do |user|  
      MailSender.with(user: user).send_every_week.deliver_now  
    end
  end
end
