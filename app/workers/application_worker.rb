class ApplicationWorker
    require 'sidekiq-scheduler'
    include Sidekiq::Worker
end