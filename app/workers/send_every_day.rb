class SendEveryDay < ApplicationWorker
  sidekiq_options queue: "mailer"
  
  def perform
    User.where(subscription: "every_day").find_each do |user|  
      MailSender.with(user: user).send_every_day.deliver_now  
    end
  end 
end
