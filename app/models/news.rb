class News < ApplicationRecord
    belongs_to :category
    validates :title, :text, presence: true
    validates :title, length: { maximum: 50 }
end
