class Category < ApplicationRecord
    has_many :news, dependent: :destroy
    has_many :users, through: :subscriptions
    has_many :subscriptions, dependent: :destroy
    validates :title, presence: true, length: { maximum: 20 }, uniqueness: true
end
