class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_many :subscriptions, dependent: :destroy
  has_many :categories, through: :subscriptions
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  before_create :write_unsubscription_token

  def write_unsubscription_token
    self.unsubscription_token = SecureRandom.hex
  end
end
