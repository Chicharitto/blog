module NewsHelper
  def main_title
    if params[:controller] == 'news' && params[:action] == 'index' 
      'Газета, которую мы заслужили'
    else
      'На главную'
    end
  end
end
