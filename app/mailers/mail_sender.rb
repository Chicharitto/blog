class MailSender < ActionMailer::Base
  
    default from: 'example@example.com'
    
    def send_every_day
      @user = params[:user]
      @news = News.where(category_id: @user.category_ids, created_at: (Date.yesterday.midnight..DateTime.now)) #выбока новостей за последний день
      
      mail(to: @user.email, subject: 'Ежедневная рассылка') 
    end
  
    def send_every_week
      @user = params[:user]
      @news = News.where(category_id: @user.category_ids, created_at: (DateTime.current.midnight.weeks_ago(1)..DateTime.now)) #выбока новостей за последнюю неделю
      
      mail(to: @user.email, subject: 'Еженедельная рассылка')
    end
  end