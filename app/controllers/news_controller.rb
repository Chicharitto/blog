class NewsController < ApplicationController
  before_action :set_news, only: [:show]

  # GET /news
  # GET /news.json
  def index
    @news = News.order("created_at DESC")
    if params[:filter]
      @news = News.where(category_id: params[:filter][:id]).order("created_at DESC")
    end
  end

  # GET /news/1
  # GET /news/1.json
  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_news
      @news = News.find(params[:id])
    end
end
