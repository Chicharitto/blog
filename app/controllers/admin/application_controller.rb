class Admin::ApplicationController < ApplicationController
  before_action :is_admin
  
  private
  def is_admin
    unless current_user.present? && current_user.is_admin? 
      (render file: 'public/404', status: 404, formats: [:html])
    end
  end
end