class Admin::DashboardController < Admin::ApplicationController
 
  def dashboard
  end

  def news_list
    @news = News.order("created_at DESC")
  end

end