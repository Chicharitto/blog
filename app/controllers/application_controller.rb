class ApplicationController < ActionController::Base

    before_action :configure_permitted_parameters, if: :devise_controller?


    def after_sign_in_path_for(resource)
        current_user.sign_in_count > 1 ? root_path : new_subscription_path
    end

    def after_sign_out_path_for(resource_or_scope)
        root_path
    end

    protected
    #разрешаем редактировать поле subscription таблицы users
    def configure_permitted_parameters
        attributes = [:subscription, :unsubscription_token]
        devise_parameter_sanitizer.permit(:sign_up, keys: attributes)
        devise_parameter_sanitizer.permit(:account_update, keys: attributes)
     end
   
end
