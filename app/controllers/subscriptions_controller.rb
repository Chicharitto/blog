class SubscriptionsController < ApplicationController
  before_action :is_admin 

  def new
    @subscription = Subscription.new
  end
    
  def create
    @subscription = Subscription.new
    respond_to do |format|
      if sub_by_category
        format.html { redirect_to root_path }
        format.json { render :show, status: :created, location: @root_path }
      else
        format.html { redirect_to new_subscription_path, :notice => 'Выберите минимум одну категорию' }
      end
    end
  end

  private

  def sub_by_category
    @subscription.transaction do
      current_user.subscriptions.delete_all
      current_user.subscription = params[:user][:subscription]  
      
      if params[:category_ids].present? && params[:user][:subscription] != "not"
        current_user.category_ids = params[:category_ids]   
      elsif params[:user][:subscription] != "not"
        raise ActiveRecord::Rollback
      end
      current_user.save!
    end
  end
 
  def is_admin
    unless current_user.present? 
      (render file: 'public/404', status: 404, formats: [:html])
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def subscription_params
    params.require(:subscription).permit(:user_id, :category_id)
  end
end