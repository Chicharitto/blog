class MyDevise::RegistrationsController < Devise::RegistrationsController
  def destroy
    user = User.find_by(id: params[:format])
    user.destroy
    redirect_to admin_dashboard_url 
  end
end