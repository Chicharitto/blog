class MailsController < ApplicationController
  def unsubscription
    user = User.find_by(unsubscription_token: params[:unsubscription_token])
    user.subscriptions.delete_all
    user.subscription = "not"  
    user.save
  end
end