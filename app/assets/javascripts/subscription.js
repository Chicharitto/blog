$(window).on("load", function(){

  var all_checkboxes= $('.custom-checkbox :checkbox').length;
  var checked_checkboxes = $('.custom-checkbox :checked').length;

  //скрывает блок чекбоксов после загрузки страницы если селектор = "Без подписки"
  $(function() {
    if ($('.custom-select').val() == "not"){
      $(".checkboxes").hide();
    }
    else {
      $(".checkboxes").show();
    }

    //выделяет чекбокс "Выделить все", если выделены все категории при загрузке страницы
    if (checked_checkboxes == all_checkboxes)
      $("#check_all").prop('checked', true);
    else 
      $("#check_all").prop('checked', false);
  });
  
  //выделяет чекбокс "Выделить все", если выделены все категории на странице
  $('.custom-checkbox :checkbox').click(function(e){
    var checked_checkboxes = $('.custom-checkbox :checked').length;
  
    if (checked_checkboxes == all_checkboxes)
      $("#check_all").prop('checked', true);
    else 
      $("#check_all").prop('checked', false);
  });
  
  //скрывает блок чекбоксов при выборе селектора "Без подписки"
  $('.custom-select').click(function(){
    if ($(this).val() == "not"){
      $(".checkboxes").hide();
    }
    else {
      $(".checkboxes").show();
    }
  });

  //Выделяет все чекбоксы при активации чекбокса "Выделить все"
  $("#check_all").click(function () {
    if (!$("#check_all").is(":checked"))
      $("input[type=checkbox]").prop('checked', false);
    else 
      $("input[type=checkbox]").prop('checked', true);
    }
  );
})